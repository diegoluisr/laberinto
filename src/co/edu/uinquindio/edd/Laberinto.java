package co.edu.uinquindio.edd;

import java.util.ArrayList;

public class Laberinto {
    private int[][] matrix;
    private int[][] backtrack;

    private int prevBacktrackX;
    private int prevBacktrackY;

    private PuntoInteres inicio;
    private PuntoInteres fin;
    private PuntoInteres cursor;
    private Direction prevDirection = null;

    private ArrayList<PuntoInteres> callejones;

    // https://theasciicode.com.ar/extended-ascii-code/box-drawings-double-line-horizontal-vertical-character-ascii-code-206.html
    private String doubleLines = "╦╣╩╠═║╚╔╗╝╨╡╥╞╬";

    public Laberinto(int[][] matrix) {
        this.matrix = matrix;
        this.backtrack = new int[matrix.length][matrix[0].length];
        this.cursor = new PuntoInteres(0, 0);
        this.prevBacktrackX = 0;
        this.prevBacktrackY = 0;
    }

    public ArrayList<PuntoInteres> obtenerBifurcaciones() {
        ArrayList<PuntoInteres> result = new ArrayList<>();
        int[] bifurcaciones = {1, 2, 3, 4};
        for (int i = 0; i < matrix.length ; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (inArray(matrix[i][j], bifurcaciones)) {
                    result.add(new PuntoInteres(j, i));
                }
            }
        }
        return result;
    }

    public ArrayList<PuntoInteres> obtenerCallejones() {
        if (this.callejones != null) {
            return this.callejones;
        }
        ArrayList<PuntoInteres> result = new ArrayList<>();
        int[] bifurcaciones = {11, 12, 13, 14};
        for (int i = 0; i < matrix.length ; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (inArray(matrix[i][j], bifurcaciones)) {
                    result.add(new PuntoInteres(j, i));
                }
            }
        }
        this.callejones = result;
        return result;
    }

    private boolean puedeMoverDerecha() {
        int[] valores = {1, 3, 4, 5, 7, 8, 14};
        if (this.inArray(matrix[cursor.getPosY()][cursor.getPosX()], valores)) {
            if (this.cursor.getPosX() + 1 < matrix[cursor.getPosY()].length) {
                return true;
            }
        }
        return false;
    }

    private void moverDerecha() {
        if (puedeMoverDerecha()) {
            System.out.println("Mueve valor:" + matrix[cursor.getPosY()][cursor.getPosX()]);
            cursor.setPosX(cursor.getPosX() + 1);
        }
    }

    private boolean puedeMoverIzquierda() {
        int[] valores = {1, 2, 3, 5, 9, 10, 12};
        if (this.inArray(matrix[cursor.getPosY()][cursor.getPosX()], valores)) {
            if (cursor.getPosX() - 1 >= 0) {
                return true;
            }
        }
        return false;
    }

    private void moverIzquierda() {
        if (puedeMoverIzquierda()) {
            System.out.println("Mueve valor:" + matrix[cursor.getPosY()][cursor.getPosX()]);
            cursor.setPosX(cursor.getPosX() - 1);
        }
    }

    private boolean puedeMoverArriba() {
        int[] valores = {2, 3, 4, 6, 7, 10, 11};
        if (this.inArray(matrix[cursor.getPosY()][cursor.getPosX()], valores)) {
            return true;
        }
        return false;
    }

    private void moverArriba() {
        if (puedeMoverArriba()) {
            System.out.println("Mueve valor:" + matrix[cursor.getPosY()][cursor.getPosX()]);
            cursor.setPosY(cursor.getPosY() - 1);
        }
    }

    private boolean puedeMoverAbajo() {
        int[] valores = {1, 2, 4, 6, 8, 9, 13};
        if (this.inArray(matrix[cursor.getPosY()][cursor.getPosX()], valores)) {
            return true;
        }
        return false;
    }

    private void moverAbajo() {
        if (puedeMoverAbajo()) {
            System.out.println("Mueve valor:" + matrix[cursor.getPosY()][cursor.getPosX()]);
            cursor.setPosY(cursor.getPosY() + 1);
        }
    }

    private boolean inArray(int i, int[] arreglo) {
        for (int item: arreglo) {
            if (i == item) {
                return true;
            }
        }
        return false;
    }

    public void loop() {
        if (prevDirection != Direction.LEFT && puedeMoverDerecha()) {
            moverDerecha();
            prevDirection = Direction.RIGHT;
        } else if (prevDirection != Direction.UP && puedeMoverAbajo()) {
            moverAbajo();
            prevDirection = Direction.DOWN;
        } else if (prevDirection != Direction.RIGHT &&puedeMoverIzquierda()) {
            moverIzquierda();
            prevDirection = Direction.LEFT;
        } else if (prevDirection != Direction.DOWN && puedeMoverArriba()) {
            moverArriba();
            prevDirection = Direction.UP;
        }

        // Invertimos la direccion si llegamos a un callejon.
        ArrayList<PuntoInteres> callejones = obtenerCallejones();
        boolean invert = false;
        for (PuntoInteres punto: callejones) {
            if (punto.getPosX() == cursor.getPosX() && punto.getPosY() == cursor.getPosY()) {
                invert = true;
            }
        }
        if (invert) {
            System.out.println("Callejon");
            if (prevDirection == Direction.RIGHT) {
                prevDirection = Direction.LEFT;
            }
            else if (prevDirection == Direction.DOWN) {
                prevDirection = Direction.UP;
            }
            else if (prevDirection == Direction.LEFT) {
                prevDirection = Direction.RIGHT;
            }
            else if (prevDirection == Direction.UP) {
                prevDirection = Direction.DOWN;
            }
        }

        System.out.print(cursor);
        System.out.print(getLabyrint(cursor));

        // Agregamos una espera/delay para validar el proceso.
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Siguente iteracion.
        loop();
    }

    public String getLabyrint(PuntoInteres punto) {
        String labyrint = "";
        char[] chars = doubleLines.toCharArray();

        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                if (punto != null && punto.getPosX() == j && punto.getPosY() == i) {
                    labyrint += "·";
                }
                else {
                    labyrint += chars[this.matrix[i][j] - 1];
                }
            }
            labyrint += "\n";
        }
        return labyrint;
    }

    public String getString(){
        String lab = "";
        char[] chars = doubleLines.toCharArray();
        for (int[] line: this.matrix) {
            for (int cell: line) {
                if (cell - 1 < chars.length) {
                    lab += chars[cell - 1];
                }
            }
            lab += "\n";
        }
        return lab;
    }

    public PuntoInteres getInicio() {
        return inicio;
    }

    public void setInicio(PuntoInteres inicio) {
        this.inicio = inicio;
    }

    public PuntoInteres getFin() {
        return fin;
    }

    public void setFin(PuntoInteres fin) {
        this.fin = fin;
    }
}
