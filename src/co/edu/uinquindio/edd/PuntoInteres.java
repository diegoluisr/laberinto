package co.edu.uinquindio.edd;

public class PuntoInteres {

    private int posX = 0;
    private int posY = 0;

    public PuntoInteres(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public String toString() {
        return "PosX: " + this.posX + " PosY: " + this.posY + "\n";
    }
}
