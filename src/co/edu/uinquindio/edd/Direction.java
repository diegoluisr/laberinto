package co.edu.uinquindio.edd;

public enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN
}
